<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>TIMELOG&copy;</title>
<style>
.logout {
	width: 15%;
	float: right;
	font-size: 16px;
	text-align: right;
	
	}
.home {
	width: 15%;
	float: left;
	font-size: 16px;
	text-align: left;
}
.header {
  padding: 1px;
  text-align: center;
}

p {
	text-align: center;
	font-size: 23px;
}
form {
	border: 2px solid black;
	background-color: lightgrey;
	margin: auto;
	width: 350px;
	text-align: center;
	padding: 8px;
	font-size: 23px;
	font-style: oblique;
}
</style>
</head>
<body>
	<div class="home">
		<a href="administrator-cubicle.jsp">Cabinet</a>
	</div>
	<div class="logout">
		<a href="Logout">Log out of here!</a>
	</div>
	<div class="header">
	<h1>Edit activity</h1>
	<hr>
	</div>
	<p>
	<form action="UpdateActivity" method="post">
	
		<input type="hidden" name="activityId" value="${param.activityId}"/>
		<p>Edit activity name:</p>
		<input type="text" name="activityName" value="${param.activityName}" style="font-size: 20px; text-align: center;">
		<p>Activate/deactivate:</p>
		<c:choose>
    		<c:when test="${param.available==true}">
        		<input type="checkbox" name="isAvailable" value="true" checked="checked" style="width:25px;height:25px;">
    		</c:when>    
    		<c:otherwise>
        		<input type="checkbox" name="isAvailable" value="true" style="width:25px;height:25px;"> 
	   		</c:otherwise>
		</c:choose>
		<br>
		<br>
		<input type="submit" value="Update activity"
			style="padding: 6px;" />
		<p>
	</form>

</body>
</html>