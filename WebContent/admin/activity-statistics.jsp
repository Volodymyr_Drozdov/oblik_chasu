<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>TIMELOG&copy;</title>
<style>
.logout {
	width: 15%;
	float: right;
	font-size: 16px;
	text-align: right;
}

.home {
	width: 15%;
	float: left;
	font-size: 16px;
	text-align: left;
}

.header {
	padding: 1px;
	text-align: center;
}

p {
	text-align: center;
	font-size: 23px;
}

table {
	width: 95%;
}

table, th {
	border: 1px solid black;
	border-collapse: collapse;
	background-color: darkgrey;
	margin: auto;
	font-style: oblique;
	padding: 8px;
	text-align: center;
	font-size: 23px;
}

table, td {
	border: 1px solid black;
	background-color: lightgrey;
	margin: auto;
	font-size: 23px;
	font-style: normal;
	padding: 8px;
}
</style>

</head>
<body>
	<div class="home">
		<a href="administrator-cubicle.jsp">Cabinet</a>
	</div>
	<div class="logout">
		<a href="Logout">Log out of here!</a>
	</div>
	<div class="header">
		<h1>Statistics for activities</h1>
	</div>
	
	
	
	
	<form action="activity-statistics.jsp" method="get"
		style="margin: auto; text-align: center;">
		<p>
			<label>Filter by category</label> 
			<select name="categoryId" onchange="this.form.submit()"
				style="font-size: 18px; background: white; padding: 2px;">
				<option id="0" selected="selected" value="0">
					All categories
				</option>
				<c:forEach var="category" items="${allCategories}">


					<option id="<c:out value = "${category.getId()}"/>" value="${category.getId()}">
						<c:out value="${category.getName()}" />
					</option>


				</c:forEach>
			</select>
		</p>

	</form>
	<br>

	<table>
		<tr>
			<th>
				<form action="activity-statistics.jsp" method="get">
					<input type="hidden" name="sort" value="category" /> <input
						type="submit" value="category">

				</form>
			</th>
			<th>
				<form action="activity-statistics.jsp" method="get">
					<input type="hidden" name="sort" value="activity" /> <input
						type="submit" value="activity">

				</form>
			</th>
			<th>
				<form action="activity-statistics.jsp" method="get">
					<input type="hidden" name="sort" value="users" /> <input
						type="submit" value="number of users">

				</form>
			</th>
		</tr>
		<c:forEach var="aStats" items="${sessionScope.filteredActivityStats}">
			<tr>
				<td>${aStats.getCategory().getName()}</td>
				<td>${aStats.getActivity().getName()}</td>
				<td>${aStats.getActivity().getNumberOfUsers()}</td>
		</c:forEach>
	</table>
	
	<script>
		document.getElementById("<c:out value = "${sessionScope.filterCategoryId}"/>").selected = "true";
	</script>
</body>
</html>