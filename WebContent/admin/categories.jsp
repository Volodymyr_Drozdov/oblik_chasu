<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>TIMELOG&copy;</title>
<style>
.logout {
	width: 15%;
	float: right;
	font-size: 16px;
	text-align: right;
	}
.home {
	width: 15%;
	float: left;
	font-size: 16px;
	text-align: left;
}
.header {
  padding: 1px;
  text-align: center;
}

p {
	text-align: center;
	font-size: 23px;
}

table {
	width: 370px;
}

table, th {
	border: 1px solid black;
	border-collapse: collapse;
	background-color: darkgrey;
	margin: auto;
	font-style: oblique;
	padding: 8px;
	text-align: center;
	font-size: 23px;
}

table, td {
	border: 1px solid black;
	background-color: lightgrey;
	margin: auto;
	font-size: 23px;
	font-style: normal;
	padding: 8px;
}

form {
	border: 2px solid black;
	background-color: lightgrey;
	margin: auto;
	width: 350px;
	text-align: center;
	padding: 8px;
	font-size: 23px;
	font-style: oblique;
}
.content {

}
</style>

</head>
<body>
	<div class="home">
		<a href="administrator-cubicle.jsp">Cabinet</a>
	</div>
	<div class="logout">
		<a href="Logout">Log out of here!</a>
	</div>
	<div class="header">
		<h1>For your viewing pleasure, list of all categories</h1>
		<h2>with activities that belong to them</h2>
	</div>
	<div class="content">
	<hr>
	<p>Activities available to users</p>
	<table>
		<c:forEach var="category" items="${allCategories}">
			<tr>
				<th>
					<a href="edit-category.jsp?categoryId=${category.getId()}&categoryName=${category.getName()}&available=true">
					<c:out value="${category.getName()}" />
					</a>
				</th>
			</tr>
			<c:forEach var="activity" items="${allActivitiesList}">
			<c:if test = "${activity.getCategory() == category.getId()}">
			<c:if test = "${activity.isAvailable() == true}">
				<tr>
				<td>
					<c:out value="${activity.getName()}" />
				</td>
				</tr>
			</c:if>
			</c:if>
			</c:forEach>
		</c:forEach>
	</table>
	
	<br>
	<p>Deactivated activities</p>
	<table>
		<c:forEach var="category" items="${allCategories}">
			<tr>
				<th>
					<a href="edit-category.jsp?categoryId=${category.getId()}&categoryName=${category.getName()}&available=false">
					<c:out value="${category.getName()}" />
					</a>
				</th>
			</tr>
			<c:forEach var="activity" items="${allActivitiesList}">
			<c:if test = "${activity.getCategory() == category.getId()}">
			<c:if test = "${activity.isAvailable() == false}">
				<tr>
				<td>
					<c:out value="${activity.getName()}" />
				</td>
				</tr>
			</c:if>
			</c:if>
			</c:forEach>
		</c:forEach>

	</table>
	</div>
</body>
</html>