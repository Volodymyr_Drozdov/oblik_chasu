<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>TIMELOG&copy;</title>

<style>
.logout {
	width: 15%;
	float: right;
	font-size: 16px;
	text-align: right;
	}
.home {
	width: 15%;
	float: left;
	font-size: 16px;
	text-align: left;
}
.header {
  padding: 1px;
  text-align: center;
}

p {
	text-align: center;
	font-size: 23px;
}

form {
	text-align: center;
	margin: auto;
}

table {
	width: 70%;
}

table, th {
	border: 1px solid black;
	border-collapse: collapse;
	background-color: darkgrey;
	margin: auto;
	font-size: 21px;
	font-style: oblique;
	padding: 8px;
}

table, td {
	border: 1px solid black;
	background-color: lightgrey;
	margin: auto;
	font-size: 23px;
	font-style: normal;
	padding: 8px;
}

</style>

</head>
<body>
	<div class="home">
		<a href="administrator-cubicle.jsp">Cabinet</a>
	</div>
	<div class="logout">
		<a href="Logout">Log out of here!</a>
	</div>
	<div class="header">
	<h1>Decide activity requests' fate, oh mighty ${sessionScope.user.getLogin()}!</h1>
	<hr>
	</div>

<p>List of activity requests:</p>
	<p>
	<table>
		<tr>
			<th style="font-size: 21px;">action</th>
			<th>user</th>
			<th>activity</th>
			<th style="font-size: 21px;">action</th>
		</tr>
		<c:forEach var="aRequest" items="${activityRequests}">
			<tr>
				<td >
					<form action="DenyActivityRequest" method="post">
						<input type=submit value="deny" style="padding:8px">
						<input type="hidden" name="userId" value="${aRequest.getUser().getId()}">
						<input type="hidden" name="activityId" value="${aRequest.getActivity().getId()}">
						<input type="hidden" name="requestId" value="${activityRequests.indexOf(aRequest)}">
					</form>
				</td>
				<td><c:out value="${aRequest.getUser().getLogin()}" /></td>
				<td><c:out value="${aRequest.getActivity().getName()}" /></td>
				<td >
					<form action="ApproveActivity" method="post">
						<input type=submit value="approve" style="padding:8px">
						<input type="hidden" name="userId" value="${aRequest.getUser().getId()}">
						<input type="hidden" name="activityId" value="${aRequest.getActivity().getId()}">
						<input type="hidden" name="requestId" value="${activityRequests.indexOf(aRequest)}">
					</form>
				</td>
			</tr>
		</c:forEach>

	</table>
	<br>

</body>
</html>