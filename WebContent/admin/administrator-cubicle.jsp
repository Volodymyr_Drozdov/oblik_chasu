<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>TIMELOG&copy;</title>

<style>
.logout {
	width: 15%;
	float: right;
	font-size: 16px;
	text-align: right;
	
	}
.header {
  padding: 1px;
  text-align: center;
}

button {
	background-color: lightgray;
	padding: 12px 28px;
	font-size: 23px;
	width: 370px;
}

div {
	width: 75%;
	/*padding: 50px;*/
	margin: auto;
	text-align: center;
}
</style>

</head>

<body>

	<div class="logout">
		<a href="Logout">Log out of here!</a>
	</div>
	<div class="header">
	<h1>Welcome to your personal cabinet,
		${sessionScope.user.getLogin()}, sir!</h1>
	<br>
	<hr>
	</div>
	<div>
	<p>Manage</p>
	<hr>
	<br>
		<form action="ManageRequests" method="post">
			<button type="submit">activity requests</button>
		</form>
		<br> <br>
		<form action="ManageUsers" method="post">
			<button type="submit">users</button>
		</form>
		<br> <br>
		<form action="categories.jsp" method="post">
			<button type="submit">activities &amp; categories</button>
		</form>
		<br>
		<br>
	<hr>
	<p>View Statistics:</p>
	<hr>
	<br>
		<form action="UserStatistics" method="post">
			<button type="submit">Users</button>
		</form>
		<br> <br>
		<form action="ActivityStatistics" method="post">
			<button type="submit">Activities</button>
		</form>
	</div>
</body>
</html>