<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>TIMELOG&copy;</title>
<style>
.logout {
	width: 15%;
	float: right;
	font-size: 16px;
	text-align: right;
}

.home {
	width: 15%;
	float: left;
	font-size: 16px;
	text-align: left;
}

.header {
	padding: 1px;
	text-align: center;
}

p {
	text-align: center;
	font-size: 23px;
}

table {
	width: 95%;
}

table, th {
	border: 1px solid black;
	border-collapse: collapse;
	background-color: darkgrey;
	margin: auto;
	font-style: oblique;
	padding: 8px;
	text-align: center;
	font-size: 23px;
}

table, td {
	border: 1px solid black;
	background-color: lightgrey;
	margin: auto;
	font-size: 23px;
	font-style: normal;
	padding: 8px;
}
</style>
</head>
<body>
	<div class="home">
		<a href="administrator-cubicle.jsp">Cabinet</a>
	</div>
	<div class="logout">
		<a href="Logout">Log out of here!</a>
	</div>
	<div class="header">
	<c:set var="index" value="${param.statsIndex}" />
		<h1>${sessionScope.userStats.get(index).getUser().getLogin()}</h1>
	</div>

	<table>
		<tr>
			<th>activity</th>
			<th>hours spent</th>

		</tr>
		<c:forEach var="entry"
			items="${sessionScope.userStats.get(index).getHoursPerActivity().entrySet()}">
			<tr>
				<td>${entry.getKey().getName()}</td>
				<td>${entry.getValue()}</td>
		</c:forEach>
	</table>

</body>
</html>