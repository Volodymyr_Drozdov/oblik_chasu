<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>TIMELOG&copy;</title>
<style>
.logout {
	width: 15%;
	float: right;
	font-size: 16px;
	text-align: right;
	}
.home {
	width: 15%;
	float: left;
	font-size: 16px;
	text-align: left;
}
.header {
  padding: 1px;
  text-align: center;
}

p {
	text-align: center;
	font-size: 23px;
}

table {
	width: 95%;
}

table, th {
	border: 1px solid black;
	border-collapse: collapse;
	background-color: darkgrey;
	margin: auto;
	font-style: oblique;
	padding: 8px;
	text-align: center;
	font-size: 23px;
}

table, td {
	border: 1px solid black;
	background-color: lightgrey;
	margin: auto;
	font-size: 23px;
	font-style: normal;
	padding: 8px;
}

</style>

</head>
<body>
	<div class="home">
		<a href="administrator-cubicle.jsp">Cabinet</a>
	</div>
	<div class="logout">
		<a href="Logout">Log out of here!</a>
	</div>
	<div class="header">
		<h1>Statistics for users</h1>
	</div>
	<table>
		<tr>
			<th>
			<form action="user-statistics.jsp" method="get">
			<input type="hidden" name="userSort" value="login"/>
			<input type="submit" value="login">
			</form>
			</th>
			<th>
			<form action="user-statistics.jsp" method="get">
			<input type="hidden" name="userSort" value="activities"/>
			<input type="submit" value="number of activities">
			
			</form>
			</th>
			<th>
			<form action="user-statistics.jsp" method="get">
			<input type="hidden" name="userSort" value="hours"/>
			<input type="submit" value="hours logged">
			
			</form>
			</th>
			<th>action</th>
		</tr>
			<c:forEach var="uStats" items="${sessionScope.userStats}">
			<tr>
				<td>${uStats.getUser().getLogin()}</td>
				<td>${uStats.getNumberOfActivities()}</td>
				<td>${uStats.getTotalHoursSpent()}</td>
				<td>
					<form action="user-statistics-detailed.jsp" method="get">
						<input type="hidden" name="statsIndex" value="${sessionScope.userStats.indexOf(uStats)}"/>
						<input type="submit" value="see details">
					</form>
				</td>
			</c:forEach>
		</table>
</body>
</html>