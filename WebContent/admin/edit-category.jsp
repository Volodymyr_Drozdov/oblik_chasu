<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>TIMELOG&copy;</title>

<style>
.logout {
	width: 15%;
	float: right;
	font-size: 16px;
	text-align: right;
	
	}
.home {
	width: 15%;
	float: left;
	font-size: 16px;
	text-align: left;
}
.header {
  padding: 1px;
  text-align: center;
}

p {
	text-align: center;
	font-size: 23px;
}

table {
	width: 370px;
}

table, th {
	border: 1px solid black;
	border-collapse: collapse;
	background-color: darkgrey;
	margin: auto;
	font-style: oblique;
	padding: 8px;
	text-align: center;
	font-size: 23px;
}

table, td {
	border: 1px solid black;
	background-color: lightgrey;
	margin: auto;
	font-size: 23px;
	font-style: normal;
	padding: 8px;
}

form {
	border: 2px solid black;
	background-color: lightgrey;
	margin: auto;
	width: 350px;
	text-align: center;
	padding: 8px;
	font-size: 23px;
	font-style: oblique;
}

</style>

</head>
<body>
	<div class="home">
		<a href="administrator-cubicle.jsp">Cabinet</a>
	</div>
	<div class="logout">
		<a href="Logout">Log out of here!</a>
	</div>
	<div class="header">
	<h1>Take a bad category, and make it better...</h1>
	<hr>
	</div>
	<p>
		<c:choose>
    		<c:when test="${param.available==true}">
        		List of activities available to users:
    		</c:when>    
    	<c:otherwise>
        		List of deactivated activities 
	    </c:otherwise>
		</c:choose>
	</p>
	<table>
			<tr>
				<th>${param.categoryName}</th>
			</tr>
			<c:forEach var="activity" items="${allActivitiesList}">
			<c:if test = "${activity.getCategory() == param.categoryId}">
			<c:if test = "${activity.isAvailable() == param.available}">
				<tr>
				<td>
					<a href="edit-activity.jsp?activityId=${activity.getId()}&activityName=${activity.getName()}&available=${param.available}">
					<c:out value="${activity.getName()}"/>
					</a>
				</td>
				</tr>
			</c:if>
			</c:if>
			</c:forEach>
	</table>
	<br>
	
	<c:if test = "${param.available == true}">
	<p>Add new activity to this category</p>
	<form action="AddNewActivity" method="post">
		<p>Enter name for the new activity</p>
		<input type="hidden" name="categoryId" value="${param.categoryId}"/>
		<input type="text" name="activityName" style="font-size: 20px;">
		<br>
		<br>
		<input type="submit" value="Add this activity"
			style="padding: 6px;" />
		<p>
	</form>
	</c:if>
</body>
</html>