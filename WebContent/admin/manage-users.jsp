<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>TIMELOG&copy;</title>

<style>
* {
  box-sizing: border-box;
  text-align: center;
}

.logout {
	width: 15%;
	float: right;
	font-size: 16px;
	text-align: right;
	
	}
.home {
	width: 15%;
	float: left;
	font-size: 16px;
	text-align: left;
}
.header {
  padding: 1px;
  text-align: center;
}

.left {
  width: 50%;
  float: left;
  padding: 15px;
}

.right {
  width: 50%;
  float: right;
  padding: 15px;
}

table, th {
	border: 1px solid black;
	border-collapse: collapse;
	background-color: darkgrey;
	margin: auto;
	font-size: 19px;
	font-style: oblique;
	padding: 8px;
}

table, td {
	border: 1px solid black;
	background-color: lightgrey;
	margin: auto;
	font-size: 21px;
	font-style: normal;
	padding: 8px;
}
p {
	text-align: center;
	font-size: 23px;
}
</style>

</head>
<body>
	<div class="home">
		<a href="administrator-cubicle.jsp">Cabinet</a>
	</div>
	<div class="logout">
		<a href="Logout">Log out of here!</a>
	</div>
	
	<div class="header">
	<h1>Judge pathetic users, oh mighty ${sessionScope.user.getLogin()}!</h1>
	<hr>
	</div>
	
	<div class="left">
		<p>List of active users</p>
		<table>
		<tr>
			<th>id</th>
			<th>login</th>
			<th>role</th>
			<th>action</th>
		</tr>
			<c:forEach var="user" items="${allActiveUsers}">
			<tr>
				<td>${user.getId()}</td>
				<td>${user.getLogin()}</td>
				<td>${user.getRole()}</td>
				<td >
					<form action="DeactivateUser" method="post">
						<input type=submit value="deactivate" style="padding:8px">
						<input type="hidden" name="userId" value="${user.getId()}">
					</form>
				</td>
			</c:forEach>
		</table>
	</div>
	
	<div class="right">
		<p>List of inactive users</p>
		<table>
		<tr>
			<th>id</th>
			<th>login</th>
			<th>role</th>
			<th>action</th>
		</tr>
			<c:forEach var="user" items="${allInactiveUsers}">
			<tr>
				<td>${user.getId()}</td>
				<td>${user.getLogin()}</td>
				<td>${user.getRole()}</td>
				<td>
					<form action="ActivateUser" method="post">
						<input type=submit value="activate" style="padding:8px">
						<input type="hidden" name="userId" value="${user.getId()}">
					</form>
				</td>
			</c:forEach>
		</table>
	</div>
</body>
</html>