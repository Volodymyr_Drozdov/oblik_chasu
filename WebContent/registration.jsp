<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>TIMELOG&copy;</title>
<style>
 div {

	background-color: lightgrey;
	width: 300px;
	border: 1px solid black;
	padding: 50px;
	margin: auto;
	text-align: center;
} 
</style>
</head>


<body>
	<div>
	<h1>Create account now!</h1>
	<hr>
	<form action="CreateUser" method="post">
		<p>Kindly provide your login:</p>
		<input type="text" name="login" style="font-size: 20px; text-align: center;" maxlength="16" required>
		<p>Password:</p>
		<input type="password" name="password" style="font-size: 20px; text-align: center;" maxlength="16" required>
		<p>Kindly repeat your password:</p>
		<input type="password" name="passwordConfirmation" style="font-size: 20px; text-align: center;"  maxlength="16" required>
		<br>
		<br>
		<input type="submit" value="create account"
			style="padding: 6px;" />
		<br>
		<p>${requestScope.warning}</p>
	</form>
	</div>
</body>
</html>