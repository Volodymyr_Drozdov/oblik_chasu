<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<fmt:setBundle basename="${sessionScope.langBundle}" var="lang"/>
<title><fmt:message key="page_title" bundle="${lang}" /></title>
<style>
p {
	text-align: center;
	font-size: 23px;
}

table {
	width: 370px;
}

table, th {
	border: 1px solid black;
	border-collapse: collapse;
	background-color: darkgrey;
	margin: auto;
	font-size: 21px;
	font-style: oblique;
	padding: 8px;
}

table, td {
	border: 1px solid black;
	background-color: lightgrey;
	margin: auto;
	font-size: 23px;
	font-style: normal;
	padding: 8px;
}

form {
	border: 2px solid black;
	background-color: lightgrey;
	margin: auto;
	width: 350px;
	text-align: center;
	padding: 8px;
	font-size: 23px;
	font-style: oblique;
}

.logout {
	width: 15%;
	float: right;
	font-size: 16px;
	text-align: right;
	}
.home {
	width: 15%;
	float: left;
	font-size: 16px;
	text-align: left;
}
.header {
  padding: 1px;
  text-align: center;
}
</style>

</head>
<body>
	<div class="home">
		<a href="user-cubicle.jsp"><fmt:message key="cubicle_link" bundle="${lang}" /></a>
	</div>
	<div class="logout">
		<a href="Logout"><fmt:message key="log_out_link" bundle="${lang}" /></a>
	</div>
	<div class=header>
	<h1><fmt:message key="history_page_header" bundle="${lang}" />${sessionScope.user.getLogin()}</h1>
	</div>
	<br>
	<table>
		<tr>
			<th><fmt:message key="timelog_table.header_id" bundle="${lang}" /></th>
			<th><fmt:message key="timelog_table.header_activity" bundle="${lang}" /></th>
			<th style="width: 30px; font-size: 14px;"><fmt:message key="timelog_table.header_time" bundle="${lang}" /></th>
		</tr>
		<c:forEach var="timelog" items="${requestScope.tList}">
			<tr>
				<td>${timelog.id}</td>
				<td><c:out
						value="${timelog.activityName}" /></td>
				<td>${timelog.time}</td>
			</tr>
		</c:forEach>

	</table>
	<br>
	<p>
	<c:forEach var="i" begin="1" end="${sessionScope.currentHistoryPage-1}">
		<a href="UserHistoryPage?currentHistoryPage=${i}"><c:out value = "${i}"/></a>
	</c:forEach>
		<c:out value = " ${sessionScope.currentHistoryPage} "/>
	<c:forEach var="i" begin="${sessionScope.currentHistoryPage + 1}" end="${sessionScope.numberOfHistoryPages}">
		<a href="UserHistoryPage?currentHistoryPage=${i}"><c:out value = "${i}"/></a>
	</c:forEach>
	</p>
</body>
</html>