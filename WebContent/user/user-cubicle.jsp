<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@page import="oblik_chasu.db.ActivityDAO"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<fmt:setBundle basename="${sessionScope.langBundle}" var="lang"/>
<title><fmt:message key="page_title" bundle="${lang}" /></title>

<style>

p {
	text-align: center;
	font-size: 23px;
}

table {
	width: 370px;
}

table, th {
	border: 1px solid black;
	border-collapse: collapse;
	background-color: darkgrey;
	margin: auto;
	font-size: 21px;
	font-style: oblique;
	padding: 8px;
}

table, td {
	border: 1px solid black;
	background-color: lightgrey;
	margin: auto;
	font-size: 23px;
	font-style: normal;
	padding: 8px;
}

form {
	border: 2px solid black;
	background-color: lightgrey;
	margin: auto;
	width: 350px;
	text-align: center;
	padding: 8px;
	font-size: 23px;
	font-style: oblique;
}


.logout {
	width: 15%;
	float: right;
	font-size: 16px;
	text-align: right;
	}
.header {
  padding: 1px;
  text-align: center;
}
</style>

</head>
<body>


	${sessionScope.message}
	<% request.getSession().removeAttribute("message"); %>
	
	<div class="logout">
		<a href="Logout"><fmt:message key="log_out_link" bundle="${lang}" /></a>
	</div>
	<div class="header">
	<h1><fmt:message key="page_header" bundle="${lang}" />${sessionScope.user.getLogin()}!</h1>

	<hr>
	</div>

	<br>
	<p>
	<p><fmt:message key="add_timelog" bundle="${lang}" /></p>

	<form action="AddTimelog" method="post">
		<p>
			<label><fmt:message key="add_timelog.form.activity_label" bundle="${lang}" /></label> <select name="activityId" style="font-size: 18px; background: white; padding: 2px;">
				<c:forEach var="userActivity" items="${sessionScope.activityList}">


					<option value="${userActivity.getId()}">
						<c:out value="${userActivity.getName()}" />
					</option>


				</c:forEach>
			</select>
		</p>
		<p>
			<label><fmt:message key="add_timelog.form.time_spent_label" bundle="${lang}" /></label> <select name="hours" style="font-size: 18px; background: white; padding: 2px;">
				<option value="0.5">0.5</option>
				<option value="1.0">1.0</option>
				<option value="1.5">1.5</option>
				<option value="2.0">2.0</option>
				<option value="2.5">2.5</option>
				<option value="3.0">3.0</option>
				<option value="3.5">3.5</option>
				<option value="4.0">4.0</option>
				<option value="4.5">4.5</option>
				<option value="5.0">5.0</option>
				<option value="5.5">5.5</option>
				<option value="6.0">6.0</option>
				<option value="6.5">6.5</option>
				<option value="7.0">7.0</option>
				<option value="7.5">7.5</option>
				<option value="8.0">8.0</option>
			</select>
		</p>
		<input type="submit" value="<fmt:message key="add_timelog.form.button" bundle="${lang}" />" style="padding: 6px;" />
		<p>
	</form>

	<br>
	<p>
	<p><fmt:message key="timelogs_list" bundle="${lang}" /></p>
	<p>
	<table>
		<tr>
			<th><fmt:message key="timelog_table.header_id" bundle="${lang}" /></th>
			<th><fmt:message key="timelog_table.header_activity" bundle="${lang}" /></th>
			<th style="width: 30px; font-size: 14px;"><fmt:message key="timelog_table.header_time" bundle="${lang}" /></th>
		</tr>
		<c:forEach var="timelog" items="${sessionScope.timelogList}">
			<tr>
				<td>${timelog.id}</td>
				<td><c:out
						value="${timelog.activityName}" /></td>
				<td>${timelog.time}</td>
			</tr>
		</c:forEach>

	</table>
	<p><a href="UserHistory"><fmt:message key="view_history.link" bundle="${lang}" /></a></p>
	<br>
	<hr>

	<br>
	<p>
	<p><fmt:message key="request_activity" bundle="${lang}" /></p>

	<form action="RequestActivity" method="post">
	<br>
	<label><fmt:message key="all_activities" bundle="${lang}" /></label>
	<p>
		<select name="activity" style="font-size: 18px; background: white; padding: 2px;">

				<c:forEach var="activity" items="${sessionScope.unavailableUserActivities}">

				<c:if test="${activity.isAvailable()==true}">
					<option value="${activity.getId()}" style="font-size: 18px; background: white; padding: 2px;">
						<c:out value="${activity.getName()}" />
					</option>
				</c:if>

				</c:forEach>
		</select> 
	</p>
		<input type="submit" value="<fmt:message key="request_activity.button" bundle="${lang}" />"
			style="padding: 6px;" />
	<p>
	</form>

	<br>
	<p>
	<p><fmt:message key="remove_activity" bundle="${lang}" /></p>

	<form action="RemoveActivity" method="post">
		<p>
			<label><fmt:message key="list_user_activities" bundle="${lang}" /></label>
		<p>
			<select name="activityId" style="font-size: 18px; background: white; padding: 2px;">
				<c:forEach var="userActivity" items="${sessionScope.activityList}">


					<option value="${userActivity.getId()}" style="font-size:18px;">
						<c:out value="${userActivity.getName()}" />
					</option>


				</c:forEach>
			</select>
		</p>

		<input type="submit" value="<fmt:message key="remove_activity.button" bundle="${lang}" />"
			style="padding: 6px;" />
		<p>
	</form>

</body>
</html>