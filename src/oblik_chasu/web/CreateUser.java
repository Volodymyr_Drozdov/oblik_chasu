package oblik_chasu.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oblik_chasu.db.UserDAO;
import oblik_chasu.db.entities.User;
import oblik_chasu.exceptions.DatabaseException;

@WebServlet("/CreateUser")
public class CreateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CreateUser() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("successful-registration.html");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter("login");
		if(login.isBlank()) {
			request.setAttribute("warning", "Login field can't be blank. Put some letters into it, we know you can!");
			request.getRequestDispatcher("registration.jsp").forward(request, response);
			return;
		}
		
		UserDAO uDao = new UserDAO();
		User user = null;
		try {
			user = uDao.findUserByLogin(login);
		} catch (DatabaseException e) {
			e.printStackTrace();
			request.getRequestDispatcher("error.html").forward(request, response);
			return;
		}
		if(user !=null) {
			request.setAttribute("warning", "This login already belongs to somebody. Find another one. Express yourself!");
			request.getRequestDispatcher("registration.jsp").forward(request, response);
			return;
		}
		
		String password = request.getParameter("password");
		if(password.isBlank()) {
			request.setAttribute("warning", "Password field can't be blank. C'mon, use your imagination!");
			request.getRequestDispatcher("registration.jsp").forward(request, response);
			return;
		}
		if(!password.equals(request.getParameter("passwordConfirmation"))) {
			request.setAttribute("warning", "You haven't confirmed your password. Try again, try harder!");
			request.getRequestDispatcher("registration.jsp").forward(request, response);
			return;
		}
		
		try {
			uDao.createUser(login, password);
		} catch (DatabaseException e) {
			e.printStackTrace();
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}
		
		doGet(request, response);
	}

}
