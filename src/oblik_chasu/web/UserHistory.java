package oblik_chasu.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oblik_chasu.db.UserDAO;
import oblik_chasu.db.entities.User;
import oblik_chasu.exceptions.DatabaseException;

@WebServlet("/UserHistory")
public class UserHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession sess = request.getSession();
		User u = (User) sess.getAttribute("user");
		
		UserDAO uDao = new UserDAO();
		int numberOfHistoryPages;
		try {
			numberOfHistoryPages = uDao.getNumberOfHistoryPages(u.getId());
		} catch (DatabaseException e) {
			e.printStackTrace();
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}
		
		sess.setAttribute("numberOfHistoryPages", numberOfHistoryPages);
		sess.setAttribute("currentHistoryPage", 1);
		
		response.sendRedirect("UserHistoryPage");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
