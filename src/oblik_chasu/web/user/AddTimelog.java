package oblik_chasu.web.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oblik_chasu.db.UserDAO;
import oblik_chasu.db.entities.Timelog;
import oblik_chasu.db.entities.User;
import oblik_chasu.exceptions.DatabaseException;

@WebServlet("/AddTimelog")
public class AddTimelog extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("user-cubicle.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession sess = request.getSession();

		User u = (User) sess.getAttribute("user");
		String param = request.getParameter("activityId");
		if (param != null) {
			long activityId = Long.parseLong(param);
			float hours = Float.parseFloat(request.getParameter("hours"));
			UserDAO uDao = new UserDAO();
			try {
				uDao.addTimelog(u.getId(), activityId, hours);
				List<Timelog> tList = uDao.findAllLogsForUserId(u.getId());
				sess.setAttribute("timelogList", tList);
			} catch (DatabaseException e) {
				e.printStackTrace();
				request.getRequestDispatcher("error.html").forward(request, response);
				return;
			}
		}
		doGet(request, response);
	}

}
