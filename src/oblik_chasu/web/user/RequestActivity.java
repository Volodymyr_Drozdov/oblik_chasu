package oblik_chasu.web.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oblik_chasu.activityRequests.ActivityRequest;
import oblik_chasu.db.ActivityDAO;
import oblik_chasu.db.entities.Activity;
import oblik_chasu.db.entities.User;
import oblik_chasu.exceptions.DatabaseException;

@WebServlet("/requestActivity")
public class RequestActivity extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RequestActivity() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.sendRedirect("user-cubicle.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession sess = request.getSession();

		User u = (User) sess.getAttribute("user");
		String param = request.getParameter("activity");
		if (param != null) {
			long activityId = Long.parseLong(param);
			ActivityDAO aDao = new ActivityDAO();
			Activity activity;
			try {
				activity = aDao.findActivityById(activityId);
			} catch (DatabaseException e) {
				e.printStackTrace();
				request.getRequestDispatcher("error.html").forward(request, response);
				return;
			}
			ActivityRequest.add(new ActivityRequest(u, activity));
			
			List<Activity> unavailableUserActivities = (List<Activity>) sess.getAttribute("unavailableUserActivities");
			Activity a = new Activity();
			a.setId(activityId);
			unavailableUserActivities.remove(a);
			sess.setAttribute("unavailableUserActivities", unavailableUserActivities);

			String message = "<script> window.alert('" + "You have requested \"" + activity.getName() + "\" activity. "
					+ "It will become available to you after approvement by administrator." + "')</script>";
			sess.setAttribute("message", message);
		}

		doGet(request, response);
	}

}
