package oblik_chasu.web.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oblik_chasu.db.UserDAO;
import oblik_chasu.db.entities.User;
import oblik_chasu.exceptions.DatabaseException;

@WebServlet("/ManageUsers")
public class ManageUsers extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ManageUsers() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("manage-users.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDAO uDao = new UserDAO();
		ServletContext context = getServletContext();
		List<User> allActiveUsers;
		try {
			allActiveUsers = uDao.findAllActiveUsers();
			context.setAttribute("allActiveUsers", allActiveUsers);
			List<User> allInactiveUsers = uDao.findAllInactiveUsers();
			context.setAttribute("allInactiveUsers", allInactiveUsers);
		} catch (DatabaseException e) {
			e.printStackTrace();
			request.getRequestDispatcher("error.html").forward(request, response);
			return;
		}

		doGet(request, response);
	}

}

