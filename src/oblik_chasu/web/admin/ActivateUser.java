package oblik_chasu.web.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oblik_chasu.db.UserDAO;
import oblik_chasu.db.entities.User;
import oblik_chasu.exceptions.DatabaseException;

@WebServlet("/ActivateUser")
public class ActivateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("manage-users.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String param = request.getParameter("userId");
		ServletContext context = getServletContext();
		if (param != null) {
			UserDAO uDao = new UserDAO();
			try {
				uDao.activateUser(Long.parseLong(param));
				List<User> allActiveUsers = uDao.findAllActiveUsers();
				context.setAttribute("allActiveUsers", allActiveUsers);
				List<User> allInactiveUsers = uDao.findAllInactiveUsers();
				context.setAttribute("allInactiveUsers", allInactiveUsers);
			} catch (NumberFormatException | DatabaseException e) {
				e.printStackTrace();
				request.getRequestDispatcher("error.html").forward(request, response);
				return;
			}

		}
		doGet(request, response);
	}

}
