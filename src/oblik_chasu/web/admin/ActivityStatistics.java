package oblik_chasu.web.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oblik_chasu.db.statistics.ActivityStats;
import oblik_chasu.exceptions.DatabaseException;

@WebServlet("/ActivityStatistics")
public class ActivityStatistics extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("activity-statistics.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		List<ActivityStats> activityStats;
		try {
			activityStats = ActivityStats.generateStatistics();
		} catch (DatabaseException e) {
			e.printStackTrace();
			request.getRequestDispatcher("error.html").forward(request, response);
			return;
		}
		HttpSession sess = request.getSession();
		sess.setAttribute("activityStats", activityStats);
		List<ActivityStats> filteredActivityStats = new ArrayList<>(activityStats);
		sess.setAttribute("filterCategoryId", 0L);
		sess.setAttribute("filteredActivityStats", filteredActivityStats);
		sess.setAttribute("orderCategoryReverse", false);
		sess.setAttribute("orderActivityReverse", false);
		sess.setAttribute("orderUserReverse", false);
		doGet(request, response);
		
	}

}
