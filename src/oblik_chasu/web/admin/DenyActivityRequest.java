package oblik_chasu.web.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oblik_chasu.activityRequests.ActivityRequest;


@WebServlet("/DenyActivityRequest")
public class DenyActivityRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DenyActivityRequest() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("ManageRequests");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ActivityRequest.remove(Integer.parseInt(request.getParameter("requestId")));
		doGet(request, response);
	}

}
