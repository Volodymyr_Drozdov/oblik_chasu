package oblik_chasu.web.admin;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import oblik_chasu.db.statistics.UserStats;

@WebFilter("/SortUserFilter")
public class SortUserFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;

		HttpSession sess = httpRequest.getSession();
		List<UserStats> userStats = (List<UserStats>) sess.getAttribute("userStats");
		
		String userSort = request.getParameter("userSort");
		if (userSort != null) {
			switch (userSort) {
			case "login":
				Collections.sort(userStats, UserStats.ByUserComparator());
				boolean reverseU = (boolean) sess.getAttribute("orderUserReverse");
				if (reverseU) Collections.reverse(userStats);
				reverseU = reverseU ? false : true;
				sess.setAttribute("orderUserReverse", reverseU);
				break;
			case "activities":
				Collections.sort(userStats, UserStats.ByActivitiesComparator());
				boolean reverseA = (boolean) sess.getAttribute("orderNumberOfActivitiesReverse");
				if (reverseA) Collections.reverse(userStats);
				reverseA = reverseA ? false : true;
				sess.setAttribute("orderNumberOfActivitiesReverse", reverseA);
				break;
			case "hours":
				Collections.sort(userStats, UserStats.ByHoursComparator());
				boolean reverseH = (boolean) sess.getAttribute("orderHoursReverse");
				if (reverseH) Collections.reverse(userStats);
				reverseH = reverseH ? false : true;
				sess.setAttribute("orderHoursReverse", reverseH);
				break;
			default:
				Collections.sort(userStats);
			}
		}
		sess.setAttribute("userStats", userStats);
		
		chain.doFilter(request, response);
	}


}
