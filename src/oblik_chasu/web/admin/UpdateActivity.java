package oblik_chasu.web.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oblik_chasu.db.ActivityDAO;
import oblik_chasu.db.entities.Activity;
import oblik_chasu.exceptions.DatabaseException;

@WebServlet("/UpdateActivity")
public class UpdateActivity extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UpdateActivity() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("categories.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String activityName = request.getParameter("activityName");
		Long activityId = Long.parseLong(request.getParameter("activityId"));
		String isAvailable = request.getParameter("isAvailable");
		Boolean available = false;
		if (isAvailable != null) {
			available = Boolean.parseBoolean(isAvailable);
		} 
		
		try {
			ActivityDAO.updateActivity(activityId, activityName, available);
		} catch (DatabaseException e1) {
			e1.printStackTrace();
			request.getRequestDispatcher("error.html").forward(request, response);
			return;
		}
		
		ServletContext sContext = getServletContext();
		List<Activity> allActivitiesList;
		try {
			allActivitiesList = ActivityDAO.findAllActivities();
		} catch (DatabaseException e) {
			e.printStackTrace();
			request.getRequestDispatcher("error.html").forward(request, response);
			return;
		}
		sContext.setAttribute("allActivitiesList", allActivitiesList);
		doGet(request, response);
	}

}
