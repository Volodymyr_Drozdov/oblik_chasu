package oblik_chasu.web.admin;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oblik_chasu.activityRequests.ActivityRequest;

@WebServlet("/manageRequests")
public class ManageRequests extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		getServletContext().setAttribute("activityRequests", ActivityRequest.activityRequests);
		response.sendRedirect("manage-requests.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// request.setAttribute("activityRequests", ActivityRequest.activityRequests);
		// request.getRequestDispatcher("/admin/manage-requests.jsp").forward(request,
		// response);
		// HttpSession sess = request.getSession();
		// sess.setAttribute("activityRequests", ActivityRequest.activityRequests);
		doGet(request, response);
	}

}
