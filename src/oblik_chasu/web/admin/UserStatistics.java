package oblik_chasu.web.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oblik_chasu.db.statistics.UserStats;
import oblik_chasu.exceptions.DatabaseException;

@WebServlet("/UserStatistics")
public class UserStatistics extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("user-statistics.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<UserStats> userStats;
		try {
			userStats = UserStats.generateUserStatistics();
		} catch (DatabaseException e) {
			e.printStackTrace();
			request.getRequestDispatcher("error.html").forward(request, response);
			return;
		}
		HttpSession sess = request.getSession();
		sess.setAttribute("userStats", userStats);
		sess.setAttribute("orderUserReverse", false);
		sess.setAttribute("orderNumberOfActivitiesReverse", false);
		sess.setAttribute("orderHoursReverse", false);
		doGet(request, response);
	}

}
