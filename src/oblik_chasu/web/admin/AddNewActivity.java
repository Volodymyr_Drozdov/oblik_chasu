package oblik_chasu.web.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oblik_chasu.db.ActivityDAO;
import oblik_chasu.db.entities.Activity;
import oblik_chasu.exceptions.DatabaseException;

@WebServlet("/AddNewActivity")
public class AddNewActivity extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AddNewActivity() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("categories.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String activityName = request.getParameter("activityName");
		Long categoryId = Long.parseLong(request.getParameter("categoryId"));
		try {
			ActivityDAO.addNewActivity(activityName, categoryId);
		} catch (DatabaseException e1) {
			e1.printStackTrace();
			request.getRequestDispatcher("error.html").forward(request, response);
			return;
		}
		
		ServletContext sContext = getServletContext();
		List<Activity> allActivitiesList = null;
		try {
			allActivitiesList = ActivityDAO.findAllActivities();
		} catch (DatabaseException e) {
			e.printStackTrace();
			request.getRequestDispatcher("error.html").forward(request, response);
			return;
		}
		sContext.setAttribute("allActivitiesList", allActivitiesList);
		doGet(request, response);
	}

}
