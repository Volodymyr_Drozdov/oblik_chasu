package oblik_chasu.web.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oblik_chasu.activityRequests.ActivityRequest;
import oblik_chasu.db.ActivityDAO;
import oblik_chasu.exceptions.DatabaseException;


@WebServlet("/ApproveActivity")
public class ApproveActivity extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ApproveActivity() {
        super();
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.sendRedirect("ManageRequests");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("ApproveActivity#doPost");

		Long userId = Long.parseLong(request.getParameter("userId"));
		Long activityId = Long.parseLong(request.getParameter("activityId"));
		
		ActivityDAO aDao = new ActivityDAO();
		boolean result;
		try {
			result = aDao.addUserActivity(userId, activityId);
		} catch (DatabaseException e) {
			e.printStackTrace();
			request.getRequestDispatcher("error.html").forward(request, response);
			return;
		}
		if (result==true) {
			ActivityRequest.remove(Integer.parseInt(request.getParameter("requestId")));
			System.out.println("Activity added!");
		}
		doGet(request, response);
		
		
	}

}
