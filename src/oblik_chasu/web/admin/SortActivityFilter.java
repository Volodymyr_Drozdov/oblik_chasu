package oblik_chasu.web.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import oblik_chasu.db.statistics.ActivityStats;

@WebFilter("/SortActivityFilter")
public class SortActivityFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;

		HttpSession sess = httpRequest.getSession();
		
		String filterParam = request.getParameter("categoryId");
		long filterCategoryId;
		if (filterParam == null) {
			filterCategoryId = (long) sess.getAttribute("filterCategoryId");
		} else {
			filterCategoryId = Long.parseLong(filterParam);
		}
		
		/*if(filterCategoryId == 0) {
			activityStats = (List<ActivityStats>) sess.getAttribute("filteredActivityStats");
		} else {
			activityStats = (List<ActivityStats>) sess.getAttribute("activityStats");
		}*/
		
		List<ActivityStats> activityStats = (List<ActivityStats>) sess.getAttribute("activityStats");
		
		List<ActivityStats> filteredActivityStats = new ArrayList<>(activityStats);
		if(filterCategoryId != 0) {
			filteredActivityStats = filteredActivityStats.stream()
					.filter(s -> s.getCategory().getId() == filterCategoryId).collect(Collectors.toList());
		}
		
		sess.setAttribute("filterCategoryId", filterCategoryId);
		
		
		String activitySort = request.getParameter("sort");
		if (activitySort != null) {
			switch (activitySort) {
			case "category":
				Collections.sort(filteredActivityStats);
				boolean reverseC = (boolean) sess.getAttribute("orderCategoryReverse");
				if (reverseC) Collections.reverse(filteredActivityStats);
				reverseC = reverseC ? false : true;
				sess.setAttribute("orderCategoryReverse", reverseC);
				break;
			case "activity":
				Collections.sort(filteredActivityStats, ActivityStats.ByActivityComparator());
				boolean reverseA = (boolean) sess.getAttribute("orderActivityReverse");
				if (reverseA) Collections.reverse(filteredActivityStats);
				reverseA = reverseA ? false : true;
				sess.setAttribute("orderActivityReverse", reverseA);
				break;
			case "users":
				Collections.sort(filteredActivityStats, ActivityStats.ByNumberOfUsersComparator());
				boolean reverseU = (boolean) sess.getAttribute("orderUserReverse");
				if (reverseU) Collections.reverse(filteredActivityStats);
				reverseU = reverseU ? false : true;
				sess.setAttribute("orderUserReverse", reverseU);
				break;
			default:
				Collections.sort(filteredActivityStats);
			}
		}
		
		sess.setAttribute("filteredActivityStats", filteredActivityStats);

		chain.doFilter(request, response);

	}

}
