package oblik_chasu.web;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oblik_chasu.activityRequests.ActivityRequest;
import oblik_chasu.db.ActivityDAO;
import oblik_chasu.db.UserDAO;
import oblik_chasu.db.entities.Activity;
import oblik_chasu.db.entities.Category;
import oblik_chasu.db.entities.Timelog;
import oblik_chasu.db.entities.User;
import oblik_chasu.exceptions.DatabaseException;

@WebServlet
public class Authorizator extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void init() {
	
		ServletContext sContext = getServletContext();
		
		//put all activities list into servlet context
		List<Activity> allActivitiesList = null;
		try {
			allActivitiesList = ActivityDAO.findAllActivities();
		} catch (DatabaseException e1) {
			e1.printStackTrace();
		}
		sContext.setAttribute("allActivitiesList", allActivitiesList);
		
		//put all categories list into servlet context
		List<Category> cList = null;
		try {
			cList = ActivityDAO.findAllCategories();
		} catch (DatabaseException e1) {
			e1.printStackTrace();

		}
		sContext.setAttribute("allCategories", cList);
		List<Category> cl = (List<Category>) sContext.getAttribute("allCategories");
		
		//put all activity requests list into servlet context
		try {
			URL resource = getServletContext().getResource("/WEB-INF/classes/requests.req");
			ActivityRequest.readFromFile(resource);
		} catch (MalformedURLException e) {
		e.printStackTrace();
		}
		sContext.setAttribute("activityRequests", ActivityRequest.activityRequests);

	}

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		super.service(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String login = request.getParameter("login");
		String password = request.getParameter("password");

		UserDAO uDao = new UserDAO();
		
		User user = null;
		try {
			user = uDao.findUserByLogin(login);
		} catch (DatabaseException e) {
			e.printStackTrace();
			request.getRequestDispatcher("error.html").forward(request, response);
			return;
		}
		if (user == null) {
			request.setAttribute("warning", "Wrong login, you bastard!<p>");
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		if (!user.getPassword().equals(password)) {
			request.setAttribute("warning", "Wrong password, you bastard!<p>");
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		HttpSession sess = request.getSession();
		sess.setAttribute("user", user);
		
		Locale locale = request.getLocale();
		if (locale.toString().equalsIgnoreCase("uk")) {
			sess.setAttribute("langBundle", "properties_ua");
		} else {
			sess.setAttribute("langBundle", "properties_en");
		}

		switch (user.getRole()) {
		case user:
			List<Timelog> tList = null;
			try {
				tList = uDao.findAllLogsForUserId(user.getId());
			} catch (DatabaseException e) {
				e.printStackTrace();
				request.getRequestDispatcher("error.html").forward(request, response);
				return;
			}
			sess.setAttribute("timelogList", tList);

			List<Activity> userActivitiesList;
			try {
				userActivitiesList = uDao.getAllActivitiesByUserId(user.getId());
			} catch (DatabaseException e) {
				e.printStackTrace();
				request.getRequestDispatcher("error.html").forward(request, response);
				return;
			}
			sess.setAttribute("activityList", userActivitiesList);

			List<Activity> unavailableUserActivities = new ArrayList<>();
			List<Activity> allActivitiesList = (List<Activity>) getServletContext().getAttribute("allActivitiesList");
			unavailableUserActivities.addAll(allActivitiesList);
			unavailableUserActivities.removeAll(userActivitiesList);
			sess.setAttribute("unavailableUserActivities", unavailableUserActivities);
			
			response.sendRedirect("user/user-cubicle.jsp");
			break;
		case administrator:
			response.sendRedirect("admin/administrator-cubicle.jsp");

		}

	}

	@Override
	public void destroy() {

		try {
			URL resource = getServletContext().getResource("/WEB-INF/classes/requests.req");
			ActivityRequest.writeToFile(resource);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
	}

}
