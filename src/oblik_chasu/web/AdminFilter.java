package oblik_chasu.web;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oblik_chasu.db.entities.User;

@WebFilter("/admin/*")
public class AdminFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;

		HttpSession sess = httpRequest.getSession();
		User user = (User) sess.getAttribute("user");

		HttpServletResponse httpResponse = (HttpServletResponse) response;

		if (user == null || !(user.getRole().toString().equals("administrator"))) {
			httpResponse.sendRedirect("/oblik_chasu/access-denied.html");
		} else {
			chain.doFilter(request, response);
		}

	}

}
