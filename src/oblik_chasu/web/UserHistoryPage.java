package oblik_chasu.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oblik_chasu.db.UserDAO;
import oblik_chasu.db.entities.Timelog;
import oblik_chasu.db.entities.User;
import oblik_chasu.exceptions.DatabaseException;

@WebServlet("/UserHistoryPage")
public class UserHistoryPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String tmp = request.getParameter("currentHistoryPage");
		int currentHistoryPage = 1;
		if (tmp != null) {
			currentHistoryPage = Integer.parseInt(tmp);
		}
		HttpSession sess = request.getSession();
		User u = (User) sess.getAttribute("user");
		UserDAO uDao = new UserDAO();
		List<Timelog> tList = null;
		try {
			tList = uDao.getPageUserTimelogs(u.getId(), currentHistoryPage);
		} catch (DatabaseException e) {
			e.printStackTrace();
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}
		request.setAttribute("tList", tList);
		sess.setAttribute("currentHistoryPage", currentHistoryPage);
		request.getRequestDispatcher("user-history.jsp").forward(request, response);
				
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
