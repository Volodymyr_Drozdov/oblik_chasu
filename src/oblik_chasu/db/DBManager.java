package oblik_chasu.db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import oblik_chasu.exceptions.DatabaseException;


public class DBManager {
	
	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null)
			instance = new DBManager();
		return instance;
	}
	
	public Connection getConnection() throws SQLException, DatabaseException {
		Connection con = null;
		try {
			Context initContext = new InitialContext();
			Context envContext  = (Context)initContext.lookup("java:/comp/env");
			
			DataSource ds = (DataSource)envContext.lookup("jdbc/mysql8");
			con = ds.getConnection();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new DatabaseException("Can't find datasource. Check context.xml and web.xml files", ex);
		}
		return con;
	}
	
	private DBManager() {
	}
	
	public void closeConnection(Connection con) {
		if (con==null) return; 
		try {
			con.commit();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public void rollbackAndClose(Connection con) {
		if (con==null) return;
		try {
			con.rollback();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public static void main(String[] args) {

	}
		
}

