package oblik_chasu.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import oblik_chasu.db.entities.Activity;
import oblik_chasu.db.entities.Roles;
import oblik_chasu.db.entities.User;
import oblik_chasu.exceptions.DatabaseException;
import oblik_chasu.db.entities.Timelog;

public class UserDAO {
	private static final String SQL_GET_PAGE_USER_TIMELOGS = "select `logs`.id, user_id, activity_id, activities.`name`, `logs`.`time` from `logs` "
			+ "join activities on `logs`.activity_id=activities.id "
			+ "where user_id=? "
			+ "order by `logs`.id DESC LIMIT ? offset ?;";	
	
	private static final int numberOfTimelogsPerPage = 5;
	
	private static final String SQL_GET_NUMBER_OF_USER_TIMELOGS = "select count(id) as numberOfLogs from `logs` where user_id = ?;";

	private static final String SQL_CALCULATE_HOURS_SPENT_BY_USER_PER_ACTIVITY =
			"select sum(`time`) as hoursSpent from `logs` where user_id=? and activity_id=?;";
	
	private static final String SQL_CREATE_USER = "insert into users (login, password, role, active) values (?, ?, 'user', false);";
	
	private static final String SQL_FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login=?";

	private static final String SQL_FIND_ALL_USERS = "SELECT id, login, role, active FROM users";

	private static final String SQL_FIND_ALL_ACTIVE_USERS = "SELECT id, login, role, active FROM users where active=1;";

	private static final String SQL_FIND_ALL_INACTIVE_USERS = "SELECT id, login, role, active FROM users where active=0;";

	private static final String SQL_FIND_ALL_ACTIVITIES_FOR_USER_ID = "select * from user_has_activities\r\n"
			+ "inner join activities on activities.id=user_has_activities.activity_id\r\n"
			+ "where user_has_activities.user_id=? and user_has_activities.available=1;";

	private static final String SQL_MAKE_ACTIVITY_UNAVAILABLE = "update user_has_activities set available=0 where user_id=? and activity_id=?;";

	private static final String SQL_ADD_TIMELOG = "insert into `logs`(user_id, activity_id, `time`)\r\n"
			+ " values (?, ?, ?);";

	private static final String SQL_DEACTIVATE_USER = "update users set `active`=false where id=?;";

	private static final String SQL_ACTIVATE_USER = "update users set `active`=true where id=?;";
	
	private static final String SQL_FIND_ALL_TIMELOGS_FOR_USER_ID =
			"select `logs`.id, user_id, activity_id, activities.`name`, `logs`.`time` from `logs`"
			+ " join activities on `logs`.activity_id=activities.id"
			+ " where user_id=?"
			+ " order by `logs`.id DESC limit 5";

	public User findUserByLogin(String login) throws DatabaseException {
		User user = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_USER_BY_LOGIN);
			pstmt.setString(1, login);
			rs = pstmt.executeQuery();
			if (rs.next())
				user = User.createUser(rs.getLong(1), rs.getString(2), rs.getString(3), Roles.valueOf(rs.getString(4)),
						rs.getBoolean(5));

		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
			throw new DatabaseException("Error when trying to find user by login: " + login, ex);
		} finally {
			DBManager.getInstance().closeConnection(con);
		}

		return user;
	}

	public List<User> findAllUsers() throws DatabaseException {
		List<User> uList = new ArrayList<>();

		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_USERS);

			while (rs.next()) {
				User u = User.createUser(rs.getLong("id"), rs.getString("login"), "*****",
						Roles.valueOf(rs.getString("role")), rs.getBoolean("active"));
				uList.add(u);
			}
			rs.close();
			stmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}

		return uList;
	}

	public List<Activity> getAllActivitiesByUserId(long userId) throws DatabaseException {
		List<Activity> aList = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_ALL_ACTIVITIES_FOR_USER_ID);
			pstmt.setLong(1, userId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				Activity a = new Activity();
				a.setId(rs.getLong("id"));
				a.setName(rs.getString("name"));
				a.setCategoryId(rs.getLong("category_id"));
				aList.add(a);
			}

		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}

		return aList;
	}

	public boolean addTimelog(long userId, long activityId, float hours) throws DatabaseException {
		boolean success = false;
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_ADD_TIMELOG);
			pstmt.setLong(1, userId);
			pstmt.setLong(2, activityId);
			pstmt.setFloat(3, hours);
			pstmt.execute();
			success = true;
		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}
		return success;
	}

	public void makeActivityUnavailable(long userId, long activityId) throws DatabaseException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_MAKE_ACTIVITY_UNAVAILABLE);
			pstmt.setLong(1, userId);
			pstmt.setLong(2, activityId);
			pstmt.execute();
		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}
	}

	public List<User> findAllActiveUsers() throws DatabaseException {
		List<User> uList = new ArrayList<>();

		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_ACTIVE_USERS);

			while (rs.next()) {
				User u = User.createUser(rs.getLong("id"), rs.getString("login"), "*****",
						Roles.valueOf(rs.getString("role")), rs.getBoolean("active"));
				uList.add(u);
			}

		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}

		return uList;
	}

	public List<User> findAllInactiveUsers() throws DatabaseException {
		List<User> uList = new ArrayList<>();

		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_INACTIVE_USERS);

			while (rs.next()) {
				User u = User.createUser(rs.getLong("id"), rs.getString("login"), "*****",
						Roles.valueOf(rs.getString("role")), rs.getBoolean("active"));
				uList.add(u);
			}

		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}

		return uList;
	}

	public void deactivateUser(long userId) throws DatabaseException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_DEACTIVATE_USER);
			pstmt.setLong(1, userId);
			pstmt.execute();
		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}
	}
	
	public void activateUser(long userId) throws DatabaseException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_ACTIVATE_USER);
			pstmt.setLong(1, userId);
			pstmt.execute();
		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}
	}

	public void createUser(String login, String password) throws DatabaseException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_CREATE_USER);
			pstmt.setString(1, login);
			pstmt.setString(2, password);
			pstmt.execute();
		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}
	}
	
	public List<Timelog> findAllLogsForUserId(long user_id) throws DatabaseException {
        List<Timelog> tList = new ArrayList<>();
        Timelog timelog = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_FIND_ALL_TIMELOGS_FOR_USER_ID);
            pstmt.setLong(1, user_id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                timelog = Timelog.createTimelog(rs.getLong(1),
                		rs.getLong(2),
                		rs.getLong(3),
                		rs.getString("name"),
                		rs.getFloat(5));
                tList.add(timelog);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().closeConnection(con);
            ex.printStackTrace();
        } finally {
        	DBManager.getInstance().closeConnection(con);
        }
        
        return tList;
    }
	
	public float calculateUserHoursPerActivity(long userId, long activityId) throws DatabaseException {
		float hoursSum = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_CALCULATE_HOURS_SPENT_BY_USER_PER_ACTIVITY);
			pstmt.setLong(1, userId);
			pstmt.setLong(2, activityId);
			rs = pstmt.executeQuery();
			if (rs.next())
				hoursSum = rs.getFloat("hoursSpent");

		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}
		return hoursSum;
	}
	
	public int getNumberOfHistoryPages(long userId) throws DatabaseException {
		int numberOfRows = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_GET_NUMBER_OF_USER_TIMELOGS);
			pstmt.setLong(1, userId);
			rs = pstmt.executeQuery();
			if (rs.next())
				numberOfRows = rs.getInt("numberOfLogs");

		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}
		
		int n = numberOfRows / numberOfTimelogsPerPage;
		if ((numberOfRows % numberOfTimelogsPerPage)>0) n++;

		return n;
	}
	
	public List<Timelog> getPageUserTimelogs(long userId, int pageNumber) throws DatabaseException {
		List<Timelog> tList = new ArrayList<>();
        Timelog timelog = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_GET_PAGE_USER_TIMELOGS);
            pstmt.setLong(1, userId);
            pstmt.setInt(2, numberOfTimelogsPerPage);
            pstmt.setInt(3, numberOfTimelogsPerPage*(pageNumber-1));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                timelog = Timelog.createTimelog(rs.getLong(1),
                		rs.getLong(2),
                		rs.getLong(3),
                		rs.getString("name"),
                		rs.getFloat(5));
                tList.add(timelog);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().closeConnection(con);
            ex.printStackTrace();
        } finally {
        	DBManager.getInstance().closeConnection(con);
        }
        
        return tList;
	}

}

