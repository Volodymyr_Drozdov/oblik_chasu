package oblik_chasu.db.entities;

import java.io.Serializable;

import oblik_chasu.db.ActivityDAO;
import oblik_chasu.exceptions.DatabaseException;

public class Activity implements Serializable{

	private static final long serialVersionUID = 1L;
	private long id;
	private String name;
	private long categoryId;
	private String desription;
	private boolean available;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public long getCategory() {
		return categoryId;
	}
	
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	
	public String getDesription() {
		return desription;
	}
	
	public void setDesription(String desription) {
		this.desription = desription;
	}
	
	

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	public int getNumberOfUsers() throws DatabaseException {
		return ActivityDAO.countUsersForActivityId(id);
	}
	
	@Override 
	public String toString() {
		return (id +" " + name + " " + categoryId + " " + isAvailable());
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Activity))
			return false;
		else return (this.getId() == ((Activity)o).getId());
	}
	
}

