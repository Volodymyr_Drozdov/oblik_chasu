package oblik_chasu.db.entities;

public class Timelog {
	
	private long id;

	private long userId;
	
	private long activityId;
	
	private String activityName;
	
	private float time;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long user_id) {
		this.userId = user_id;
	}

	public long getActivityId() {
		return activityId;
	}

	public void setActivityId(long activity_id) {
		this.activityId = activity_id;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public float getTime() {
		return time;
	}

	public void setTime(float time) {
		this.time = time;
	}
	
	public static Timelog createTimelog(long id, long userId, long activityId, String activityName, float time) {
		Timelog timelog = new Timelog();
		timelog.setId(id);
		timelog.setUserId(userId);
		timelog.setActivityId(activityId);
		timelog.setActivityName(activityName);
		timelog.setTime(time);
		return timelog;
	}
	
	@Override
	public String toString() {
		return (id + " " + userId + " " + activityId + " " + activityName + " " + time);
	}
}

