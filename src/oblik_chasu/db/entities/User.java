package oblik_chasu.db.entities;

import java.io.Serializable;

public class User implements Serializable{

	private static final long serialVersionUID = 1L;
	private long id;
	private String login;
	private String password;
	private Roles role;
	private boolean active;

	public static User createUser(String log, String pass, Roles rol, boolean act) {
		User usr = new User();
		usr.setLogin(log);
		usr.setPassword(pass);
		usr.setRole(rol);
		usr.setActive(act);
		return usr;
	}

	public static User createUser(long id, String log, String pass, Roles rol, boolean act) {
		User usr = new User();
		usr.setId(id);
		usr.setLogin(log);
		usr.password = pass;
		usr.setRole(rol);
		usr.setActive(act);
		return usr;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Roles getRole() {
		return role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof User))
			return false;
		return this.getId() == ((User) obj).getId();
	}

	@Override
	public int hashCode() {
		return (int) id;
	}

	@Override
	public String toString() {
		return getId() + " " + getLogin() + " " + getPassword() + " " + getRole() + " " + isActive();
	}
}

