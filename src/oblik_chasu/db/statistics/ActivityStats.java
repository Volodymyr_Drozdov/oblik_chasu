package oblik_chasu.db.statistics;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import oblik_chasu.db.ActivityDAO;
import oblik_chasu.db.entities.Activity;
import oblik_chasu.db.entities.Category;
import oblik_chasu.exceptions.DatabaseException;

public class ActivityStats implements Comparable<ActivityStats> {
	private Category category;
	private Activity activity;
	public int numberOfUsers;

	public ActivityStats() {
	};

	ActivityStats(Category c, Activity a, int count) {
		category = c;
		activity = a;
		numberOfUsers = count;
	};

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public int getNumberOfUsers() {
		return numberOfUsers;
	}

	public void setNumberOfUsers(int numberOfUsers) {
		this.numberOfUsers = numberOfUsers;
	}


	public static List<ActivityStats> generateStatistics() throws DatabaseException {
		List<Category> cList = ActivityDAO.findAllCategories();
		List<Activity> allActivitiesList = ActivityDAO.findAllActivities();
		List<ActivityStats> aStats = new ArrayList<>();
		for (Activity a : allActivitiesList) {
			Category cat = cList.stream().filter(c -> c.getId() == a.getCategory()).findAny().orElse(null);
			ActivityStats aS = new ActivityStats(cat, a, a.getNumberOfUsers());
			aStats.add(aS);
		}
		return aStats;
	}

	@Override
	public int compareTo(ActivityStats o) {
		return (int) (this.getCategory().getId() - o.getCategory().getId());
	}

	public static class ByActivityComparator implements Comparator<ActivityStats> {
		@Override
		public int compare(ActivityStats st1, ActivityStats st2) {
			return st1.getActivity().getName().compareToIgnoreCase(st2.getActivity().getName());
		}
	}

	public static Comparator<ActivityStats> ByActivityComparator() {
		return new ByActivityComparator();
	}

	public static class ByNumberOfUsersComparator implements Comparator<ActivityStats> {

		@Override
		public int compare(ActivityStats st1, ActivityStats st2) {
			return Integer.compare(st1.getNumberOfUsers(), st2.getNumberOfUsers());
		}

	}
	public static Comparator<ActivityStats> ByNumberOfUsersComparator() {
		return new ByNumberOfUsersComparator();
	}

}
