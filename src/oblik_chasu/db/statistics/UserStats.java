package oblik_chasu.db.statistics;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oblik_chasu.db.UserDAO;
import oblik_chasu.db.entities.Activity;
import oblik_chasu.db.entities.User;
import oblik_chasu.exceptions.DatabaseException;

public class UserStats implements Comparable<UserStats>{
	private User user;
	private Map<Activity, Float> hoursPerActivity;

	public UserStats(User u, Map<Activity, Float> hPerA) {
		user = u;
		hoursPerActivity = hPerA;
	}
	
	
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Map<Activity, Float> getHoursPerActivity() {
		return hoursPerActivity;
	}
	
	public int getNumberOfActivities() {
		return hoursPerActivity.size();
	}
	
	public float getTotalHoursSpent() {
		return hoursPerActivity.values()
		.stream().reduce(0.0f, (a,b) -> a + b);
	}
	
	@Override
	public int compareTo(UserStats o) {
		return this.getUser().getLogin().compareToIgnoreCase(o.getUser().getLogin());
	}
	
	public static class ByUserComparator implements Comparator<UserStats> {
		@Override
		public int compare(UserStats u1, UserStats u2) {
			return u1.getUser().getLogin().compareToIgnoreCase(u2.getUser().getLogin());
		}
	}
	
	public static Comparator<UserStats> ByUserComparator() {
		return new ByUserComparator();
	}

	public static class ByNumberOfActivitiesComparator implements Comparator<UserStats> {
		@Override
		public int compare(UserStats st1, UserStats st2) {
			return Integer.compare(st1.getNumberOfActivities(), st2.getNumberOfActivities());
		}
	}

	public static Comparator<UserStats> ByActivitiesComparator() {
		return new ByNumberOfActivitiesComparator();
	}

	public static class ByHoursSpentComparator implements Comparator<UserStats> {

		@Override
		public int compare(UserStats st1, UserStats st2) {
			return Float.compare(st1.getTotalHoursSpent() , st2.getTotalHoursSpent());
		}

	}
	public static Comparator<UserStats> ByHoursComparator() {
		return new ByHoursSpentComparator();
	}


	public static List<UserStats> generateUserStatistics() throws DatabaseException {

		UserDAO uDao = new UserDAO();
		List<User> uList = uDao.findAllActiveUsers();
		List<UserStats> uStats = new ArrayList<>();

		for (User u : uList) {
			Map<Activity, Float> hPerA = new HashMap<>();
			UserStats uS = null;
			for (Activity a : uDao.getAllActivitiesByUserId(u.getId())) {		
				hPerA.put(a, uDao.calculateUserHoursPerActivity(u.getId(), a.getId()));
			}
			uS = new UserStats(u, hPerA);
			uStats.add(uS);
		}
		return uStats;
	}

	public void setHoursPerActivity(Map<Activity, Float> hoursPerActivity) {
		this.hoursPerActivity = hoursPerActivity;
	}

}
