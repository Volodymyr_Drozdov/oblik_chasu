package oblik_chasu.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import oblik_chasu.db.entities.Activity;
import oblik_chasu.db.entities.Category;
import oblik_chasu.exceptions.DatabaseException;

public class ActivityDAO {
	private static final String SQL_COUNT_USERS_FOR_ACTIVITY_ID =
			"SELECT COUNT(user_id) as number_of_users from user_has_activities where activity_id=? and available=true;";
	
	private static final String SQL_FIND_ALL_CATEGORIES = "SELECT * FROM categories";
	
	private static final String SQL_FIND_ALL_ACTIVITIES = "SELECT * FROM activities order by category_id";
	
	private static final String SQL_FIND_ALL_ACTIVITIES_BY_CATEGORY = "SELECT * FROM activities where category_id=?";

	private static final String SQL_FIND_ACTIVITY_BY_ID = "SELECT * FROM activities WHERE id=?";
	
	private static final String SQL_ADD_NEW_ACTIVITY = 
			"insert into activities (`name`, `category_id`) values (?, ?)";

	private static final String SQL_FIND_USER_HAS_ACTIVITY = "select * from user_has_activities where user_id=? and activity_id=?;";

	private static final String SQL_MAKE_ACTIVITY_AVAILABLE = "update user_has_activities set available=1 where user_id=? and activity_id=?;";

	private static final String SQL_INSERT_USER_ACTIVITY = "insert into user_has_activities (user_id, activity_id, available) values (?, ?, true)";
	
	private static final String SQL_UPDATE_ACTIVITY = "update activities set `name`=?, available=? where id=?;";
	private static final String SQL_UPDATE_USER_HAS_ACTIVITY = "update user_has_activities set available=0 where activity_id=?;";

	public static List<Category> findAllCategories() throws DatabaseException {
		List<Category> cList = new ArrayList<>();

		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_CATEGORIES);

			while (rs.next()) {
				Category c = new Category();
				c.setId(rs.getLong("id"));
				c.setName(rs.getString("name"));
				cList.add(c);
			}
		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}

		return cList;
	}
	
	public static List<Activity> findAllActivities() throws DatabaseException {
		List<Activity> aList = new ArrayList<>();

		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_ACTIVITIES);

			while (rs.next()) {
				Activity a = new Activity();
				a.setId(rs.getLong("id"));
				a.setName(rs.getString("name"));
				a.setCategoryId(rs.getLong("category_id"));
				a.setAvailable(rs.getBoolean("available"));
				aList.add(a);
			}
		} catch (Exception ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
			throw new DatabaseException("Can't get all activitites list", ex);
		} finally {
			DBManager.getInstance().closeConnection(con);
		}

		return aList;
	}
	
	public static List<Activity> findAllActivitiesByCategory(long categoryId) throws DatabaseException {
		List<Activity> aList = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;

		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_ALL_ACTIVITIES_BY_CATEGORY);
			pstmt.setLong(1, categoryId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				Activity a = new Activity();
				a.setId(rs.getLong("id"));
				a.setName(rs.getString("name"));
				a.setCategoryId(rs.getLong("category_id"));
				a.setAvailable(rs.getBoolean("available"));
				aList.add(a);
			}

		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}

		return aList;
	}

	public Activity findActivityById(Long id) throws DatabaseException {
		Activity activity = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_ACTIVITY_BY_ID);
			pstmt.setLong(1, id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				activity = new Activity();
				activity.setId(rs.getLong("id"));
				activity.setName(rs.getString("name"));
				activity.setCategoryId(rs.getLong("category_id"));
				activity.setAvailable(rs.getBoolean("available"));
			}
		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}

		return activity;
	}

	public boolean addUserActivity(Long userId, Long activityId) throws DatabaseException {
		boolean success = false;
		PreparedStatement pstmt = null;
		Connection con = null;
		ResultSet resultSet = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_USER_HAS_ACTIVITY);
			pstmt.setLong(1, userId);
			pstmt.setLong(2, activityId);
			resultSet = pstmt.executeQuery();
			if (resultSet.next()) {
				pstmt = con.prepareStatement(SQL_MAKE_ACTIVITY_AVAILABLE);
				pstmt.setLong(1, userId);
				pstmt.setLong(2, activityId);
				pstmt.execute();
				success = true;
			} else {
				pstmt = con.prepareStatement(SQL_INSERT_USER_ACTIVITY);
				pstmt.setLong(1, userId);
				pstmt.setLong(2, activityId);
				pstmt.execute();
				success = true;
			}
		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}

		return success;
	}

	public static void addNewActivity(String name, long categoryId) throws DatabaseException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_ADD_NEW_ACTIVITY);
			pstmt.setString(1, name);
			pstmt.setLong(2, categoryId);
			pstmt.execute();
		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}
	}
	
	public static void updateActivity(long id, String name, boolean available) throws DatabaseException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_ACTIVITY);
			pstmt.setString(1, name);
			pstmt.setBoolean(2, available);
			pstmt.setLong(3, id);
			pstmt.execute();
			if(available==false) {
			pstmt = con.prepareStatement(SQL_UPDATE_USER_HAS_ACTIVITY);
			pstmt.setLong(1, id);
			pstmt.execute();
			}
		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}
	}
	
	public static int countUsersForActivityId (long activityId) throws DatabaseException {
		int count = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_COUNT_USERS_FOR_ACTIVITY_ID);
			pstmt.setLong(1, activityId);
			rs = pstmt.executeQuery();
			if (rs.next())
				count = rs.getInt("number_of_users");

		} catch (SQLException ex) {
			DBManager.getInstance().closeConnection(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().closeConnection(con);
		}

		return count;
	}
	
}

