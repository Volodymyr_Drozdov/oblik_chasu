package oblik_chasu.exceptions;

public class DatabaseException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public DatabaseException(String errorMessage, Throwable error) {
		super(errorMessage, error);
	}

}
