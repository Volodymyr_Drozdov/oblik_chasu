package oblik_chasu.activityRequests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import oblik_chasu.db.entities.Activity;
import oblik_chasu.db.entities.User;

public class ActivityRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	public static volatile List<ActivityRequest> activityRequests;

	private User user;
	private Activity activity;

	public ActivityRequest(User u, Activity a) {
		this.user = u;
		this.activity = a;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	@Override
	public String toString() {
		return user.getLogin() + " requests \"" + activity.getName() + "\"";
	}

	public static synchronized void add(ActivityRequest ar) {
		if (activityRequests == null)
			activityRequests = new ArrayList<>();
		activityRequests.add(ar);
	}

	public static synchronized void remove(int index) {
		activityRequests.remove(index);
	}
	
	public static void readFromFile(URL fileUrl) {
		activityRequests = new ArrayList<>();
		
		if (fileUrl == null) {
			System.out.println("can't find 'requests.req' file!");
			return;
		}
		
		File file = new File(fileUrl.getPath());
		
		
		
		if (file.exists() && !file.isDirectory()) {
			ObjectInputStream objInput;
			FileInputStream fileInput;
			try {
				fileInput = new FileInputStream(file);
				objInput = new ObjectInputStream(fileInput);

				boolean cont = true;
				while (cont) {
					if (fileInput.available() > 0) {
						ActivityRequest r = (ActivityRequest) objInput.readObject();
						activityRequests.add(r);
					} else {
						cont = false;
					}
				}
				System.out.println("requests read from file");
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}

	}

	public static void writeToFile(URL fileUrl) {
		
		if (fileUrl == null) {
			System.out.println("can't find 'requests.req' file!");
			return;
		}
		
		File file = new File(fileUrl.getPath());
		ObjectOutputStream objOut = null;

		try {
			FileOutputStream fileOut = new FileOutputStream(file);
			objOut = new ObjectOutputStream(fileOut);

			for (ActivityRequest ar : activityRequests) {
				objOut.writeObject(ar);
			}
			System.out.println("requests saved!");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				objOut.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) {

		/*UserDAO uDao = new UserDAO();
		User u1 = uDao.findUserByLogin("salesman");
		ActivityDAO aDao = new ActivityDAO();
		Activity a1 = aDao.findActivityById(41L);
		ActivityRequest ar = new ActivityRequest(u1, a1);
		add(ar);
		u1 = uDao.findUserByLogin("itguy");
		a1 = aDao.findActivityById(31L);
		ar = new ActivityRequest(u1, a1);
		add(ar);
		System.out.println(activityRequests);

		writeToFile();

		readFromFile();
		System.out.println(activityRequests);*/

	}
}
