-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema timelogdb8
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `timelogdb8` ;

-- -----------------------------------------------------
-- Schema timelogdb8
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `timelogdb8` DEFAULT CHARACTER SET utf8 ;
USE `timelogdb8` ;

-- -----------------------------------------------------
-- Table `timelogdb8`.`categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `timelogdb8`.`categories` ;

CREATE TABLE IF NOT EXISTS `timelogdb8`.`categories` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32) NOT NULL,
  `description` VARCHAR(113) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `timelogdb8`.`activities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `timelogdb8`.`activities` ;

CREATE TABLE IF NOT EXISTS `timelogdb8`.`activities` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32) NOT NULL,
  `category_id` INT UNSIGNED NOT NULL,
  `description` VARCHAR(113) NULL DEFAULT NULL,
  `available` TINYINT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_activities_categories_idx` (`category_id` ASC) VISIBLE,
  CONSTRAINT `fk_activities_categories`
    FOREIGN KEY (`category_id`)
    REFERENCES `timelogdb8`.`categories` (`id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `timelogdb8`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `timelogdb8`.`users` ;

CREATE TABLE IF NOT EXISTS `timelogdb8`.`users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(16) NOT NULL,
  `password` VARCHAR(16) NOT NULL,
  `role` ENUM('user', 'administrator') NOT NULL DEFAULT 'user',
  `active` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `timelogdb8`.`user_has_activities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `timelogdb8`.`user_has_activities` ;

CREATE TABLE IF NOT EXISTS `timelogdb8`.`user_has_activities` (
  `user_id` INT UNSIGNED NOT NULL,
  `activity_id` INT UNSIGNED NOT NULL,
  `available` TINYINT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`user_id`, `activity_id`),
  INDEX `fk_activity_id` (`activity_id` ASC) VISIBLE,
  CONSTRAINT `fk_activity_id`
    FOREIGN KEY (`activity_id`)
    REFERENCES `timelogdb8`.`activities` (`id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `timelogdb8`.`users` (`id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `timelogdb8`.`logs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `timelogdb8`.`logs` ;

CREATE TABLE IF NOT EXISTS `timelogdb8`.`logs` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `activity_id` INT UNSIGNED NOT NULL,
  `time` FLOAT NOT NULL,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_user_activity_idx` (`user_id` ASC, `activity_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_activity`
    FOREIGN KEY (`user_id` , `activity_id`)
    REFERENCES `timelogdb8`.`user_has_activities` (`user_id` , `activity_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 59
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `timelogdb8`.`user_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `timelogdb8`.`user_info` ;

CREATE TABLE IF NOT EXISTS `timelogdb8`.`user_info` (
  `id` INT UNSIGNED NOT NULL,
  `first_name` VARCHAR(16) NULL DEFAULT NULL,
  `second_name` VARCHAR(16) NULL DEFAULT NULL,
  `info` VARCHAR(1024) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  CONSTRAINT `fk_user_info`
    FOREIGN KEY (`id`)
    REFERENCES `timelogdb8`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
